package hugedata;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import hugedata.Filter.Type;

public class Data {	
	
	public static ArrayList<Filter> filters = new ArrayList<Filter>();
	public static ArrayList<Formatter> formatters = new ArrayList<Formatter>();
	
	public static String website = "http://networkrepository.com/misc.php";
	public static Document doc;
	
	public static int maxsizemegabytes = 500;
	
	public static String openDoc(String url){
		url = url.replace("https", "http");
		
		try {
			doc = Jsoup.connect(url).timeout(60000).maxBodySize(maxsizemegabytes*1024*1024).validateTLSCertificates(false).get();
		} catch (Exception e) {
			e.printStackTrace();
			return "Site Fetch Failed";
		}
		
		String title = doc.getElementsByTag("title").toString().replace("<title>", "").replace("</title>", "")+" @ ";
		
		return " "+title+doc.location();
	}

	public static String[] getList(boolean full) {
		if(doc == null)
			return new String[]{};
		
		Elements list = doc.getElementsByTag("html");
		
		for (Filter f : filters) {
			if(f.text == null || f.text.length() == 0)
				continue;
			
			if(f.type == Type.Element_ID)
			{
				Element get = doc.getElementById(f.text);
				
				if(get == null)
					list = new Elements();
				else
					list = get.getAllElements();
			}
			else
			{
				Elements newlist = new Elements();	
				for (Element element : list) {
					switch(f.type){
						case Element_Attribute:newlist.addAll(element.getElementsByAttribute(f.text));	break;
						case Element_Class: newlist.addAll(element.getElementsByClass(f.text));	break;
						case Element_Tag: newlist.addAll(element.getElementsByTag(f.text));	break;
						default:System.out.println("DAFFAQ");break;
					}
				}
				list = newlist;
			}
		}
		
		int size = list.size();
		ArrayList<String> preview = new ArrayList<String>(size);
		
		int totallines = 0;
		for (int i = 0; i < size; i++) {
			String get = format(list.get(i));
			
			if(get.length() == 0)
				continue;
			
			if(!full)
			{
				String[] split = get.split("[\\n]");
				
				int lines = split.length;
				
				totallines += lines;
				
				if(totallines > 500){
					totallines-=lines;
					
					int linesleft = 500-totallines;
					
					StringBuffer buf = new StringBuffer(split[0]);
					
					for (int j = 1; j < linesleft; j++) {
						buf.append("\n"+split[j]);
					}
					
					preview.add(buf.toString());
					
					return preview.toArray(new String[preview.size()]);
				}
				
				preview.add(get);
			}
			else
				preview.add(get);
			
		}
		return preview.toArray(new String[preview.size()]);
	}

	private static String format(Element element) {
		
		if(formatters.size() == 0)
			return element.toString();
		
		String ret = "";
		
		for (Formatter f : formatters) {
			if(f.needsString && (f.text1 == null || f.text1.length() == 0))
				continue;			
			
			switch(f.action){
				case RawText: ret = ret + element.toString() + " "; break;
				case Text: ret = ret + element.text() + " "; break;
				case Value: ret = ret + element.val() + " "; break;
				case Attribute: ret = ret + element.attr(f.text1) + " "; break;
				case Prepend: ret = f.text1+ret; break;
				case Append:ret = ret+f.text1; break;
				case RegEx:ret = ret.replaceAll(f.text1, f.text2); break;
			}
		}
		
		return ret.trim();
	}
	
	
}