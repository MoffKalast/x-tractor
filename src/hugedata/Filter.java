package hugedata;

public class Filter {
	
	public enum Type{
		Element_ID, Element_Class, Element_Attribute, Element_Tag
	}
	
	public final Type type;
	public String text;
	
	public Filter(Type type) {
		this.type = type;
	}
	
	public void setText(String s){
		text = s;
	}
	
}
