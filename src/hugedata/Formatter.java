package hugedata;

public class Formatter {
	
	public enum Action{
		RawText, Value, Text, Attribute, Prepend, Append, RegEx
	}
	
	public boolean needsString = false;
	public boolean needsString2 = false;
	public final Action action;
	public String text1, text2 = ""; 
	
	public Formatter(Action action) {
		this.action = action;
		
		switch(action)
		{
			case RegEx: this.needsString = this.needsString2 = true;
				break;
				
			case Append:
			case Attribute:
			case Prepend: this.needsString = true;
				break;
				
			case RawText:
			case Text:
			case Value:
			default: this.needsString = false;
		}
	}
	
	public void setParam1(String s){
		text1 = s;
	}
	
	public void setParam2(String s){
		text2 = s;
	}
	
}
