package hugedata;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.HashMap;

import hugedata.Filter.Type;
import hugedata.Formatter.Action;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class GUI extends Application {
	
	private HashMap<TextField, Filter> filterlinks = new HashMap<TextField, Filter>();
	private HashMap<TextField, Formatter> formatterlinks = new HashMap<TextField, Formatter>();
	
	private VBox maintable = new VBox();
	
	private void rebuild(){
		maintable.getChildren().clear();
		
		String[] list = Data.getList(false);
		
		for (int i = 0; i < list.length; i++) {
			if(list[i] == null || list[i].length() == 0)
				continue;
			
			Label lab = new Label(list[i]);
			lab.setPadding(new Insets(4));
			lab.setStyle("-fx-border-color:white; -fx-background-color: lightgrey;");
			maintable.getChildren().add(lab);
		}
	}
	
	@Override
	public void start(Stage primaryStage) {		
		BorderPane left = new BorderPane();
		left.setStyle("-fx-background-color: #555555;");
		left.setPadding(new Insets(5));
		
		VBox right = new VBox();
		right.setStyle("-fx-background-color: #555555;");
		right.setPadding(new Insets(5));
		right.setSpacing(4);
		
		SplitPane splitPane = new SplitPane(left, right);
		splitPane.setOrientation(Orientation.HORIZONTAL);

		BorderPane borderPane = new BorderPane(splitPane);
		borderPane.setTop(getTopRow());
		
		setupControls(left);
		setupView(right);
		
		Scene scene = new Scene(borderPane, 1700, 900);
		primaryStage.setTitle("X-Tractor");
		primaryStage.setScene(scene);
		primaryStage.show();
		
		rebuild();
	}
	
	private Node getTopRow() {
	    HBox firstrow = new HBox();
	    firstrow.setPadding(new Insets(5, 5, 5, 5));
	    firstrow.setSpacing(10);
	    firstrow.setStyle("-fx-background-color: #336699;");
		
	    Label url = new Label("URL:");
	    url.setTextFill(Color.WHITE);
	    url.setFont(Font.font(20));
		firstrow.getChildren().add(url);
		
		TextField textField = new TextField("https://hub.jmonkeyengine.org/");
		textField.setPrefColumnCount(50);
		textField.setFont(Font.font(15));
		firstrow.getChildren().add(textField);
	    
	    Label site = new Label(" | Nothing loaded.");
	    site.setTextFill(Color.WHITE);
	    site.setFont(Font.font(20));
		
		Button load = new Button("Load");
		load.setTextFill(Color.BLACK);
		load.setFont(Font.font(13));
		load.setOnAction((event) -> {
			load.setText("Loading");
			
			String title = Data.openDoc(textField.getText());
			
			load.setText("Load");
			
			site.setText(" | "+title);
			
			rebuild();
		}); 
				
		Button print = new Button("PrintOut");
		print.setTextFill(Color.BLACK);
		print.setFont(Font.font(13));
		print.setOnAction((event) -> {
			try {
				
				File fileDir = new File(System.getProperty("user.home") + "/Desktop/X-TractorOut.txt");
				
				PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileDir), "UTF8")));
				
				for (String s : Data.getList(true)) {
					if(s == null || s.length() == 0)
						continue;
					
					out.println(s);
					System.out.println(s);
				}
				out.close();
				
			} catch (IOException e) {
				e.printStackTrace();
			} 
		}); 
		
		firstrow.getChildren().addAll(load,site,print);
		
		return firstrow;
	}

	private void setupControls(BorderPane pane){
		GridPane gridpane1 = new GridPane();
		GridPane gridpane2 = new GridPane();
		
		VBox bottomrows = new VBox();
		bottomrows.setSpacing(30);
		
		// FILTERS
		
		HBox buttonrow = new HBox();	
		buttonrow.setPadding(new Insets(5));
		buttonrow.setSpacing(30);
		
		Label addbut = new Label("Add Filter:");
		addbut.setTextFill(Color.WHITE);
		addbut.setFont(Font.font(17));
		buttonrow.getChildren().add(addbut);
		
		for (Type filter : Filter.Type.values()) {
			Button but = new Button(filter.name().replace("_", " "));
			but.setTextFill(Color.BLACK);
			but.setFont(Font.font(12));
			but.setPadding(new Insets(5));
			but.setOnAction((event) -> {
				Filter f = new Filter(filter);
				
				int pos = 0;
				
				Label label = new Label(but.getText()+": ");
				label.setTextFill(Color.WHITE);
				label.setFont(Font.font(20));
				GridPane.setConstraints(label, pos++, Data.filters.size());
				
				TextField textfield = new TextField();
				textfield.setPrefColumnCount(30);
				textfield.setFont(Font.font(15));
				textfield.textProperty().addListener((observable, oldValue, newValue) -> {
					filterlinks.get(textfield).setText(newValue);
					rebuild();
				});
				GridPane.setConstraints(textfield, pos++, Data.filters.size());
				
				gridpane1.getChildren().addAll(label,textfield);
				
				filterlinks.put(textfield, f);				
				Data.filters.add(f);
				
				Button remove = new Button("X");
				remove.setTextFill(Color.BLACK);
				remove.setFont(Font.font(15));
				remove.setOnAction((event2) -> {
					Data.filters.remove(f);
					
					gridpane2.getChildren().removeAll(label,remove, textfield);
					formatterlinks.remove(textfield);
					gridpane2.getChildren().remove(textfield);
					
					rebuild();
				}); 
				
				textfield.requestFocus();
			}); 
			buttonrow.getChildren().add(but);
		}
		
		Button clear = new Button("Clear Filters");
		clear.setTextFill(Color.RED);
		clear.setFont(Font.font(12));
		clear.setPadding(new Insets(5));
		clear.setOnAction((event) -> {
			Data.filters.clear();
			filterlinks.clear();
			gridpane1.getChildren().clear();
			rebuild();
		}); 
		buttonrow.getChildren().add(clear);
		
		// FORMATTERS
		
		HBox formatterrow = new HBox();	
		formatterrow.setPadding(new Insets(5));
		formatterrow.setSpacing(30);
		
		Label addform = new Label("Add Formatter:");
		addform.setTextFill(Color.WHITE);
		addform.setFont(Font.font(17));
		formatterrow.getChildren().add(addform);
		
		for (Action formatter : Formatter.Action.values()) {
			Button but = new Button(formatter.name().replace("_", " "));
			but.setTextFill(Color.BLACK);
			but.setFont(Font.font(12));
			but.setPadding(new Insets(5));
			but.setOnAction((event) -> {
				Formatter f = new Formatter(formatter);
				
				int pos = 0;
				
				Label label = new Label(but.getText()+": ");
				label.setTextFill(Color.WHITE);
				label.setFont(Font.font(20));
				GridPane.setConstraints(label, pos++, Data.formatters.size());
				
				TextField textfield = null, textfield2 = null;
				
				if(f.needsString)
				{
					textfield = new TextField();
					textfield.setPrefColumnCount(25);
					textfield.setFont(Font.font(15));
					
					TextField finallink = textfield;					
					textfield.textProperty().addListener((observable, oldValue, newValue) -> {
						formatterlinks.get(finallink).setParam1(newValue);
						rebuild();
					});
					GridPane.setConstraints(textfield, pos++, Data.formatters.size());
					
					formatterlinks.put(textfield, f);	
					gridpane2.getChildren().add(textfield);
					
					textfield.requestFocus();
					
					if(f.needsString2)
					{
						textfield2 = new TextField();
						textfield2.setPrefColumnCount(20);
						textfield2.setFont(Font.font(15));
						
						TextField finallink2 = textfield2;	
						textfield2.textProperty().addListener((observable, oldValue, newValue) -> {
							formatterlinks.get(finallink2).setParam2(newValue);
							rebuild();
						});
						GridPane.setConstraints(textfield2, pos++, Data.formatters.size());
						
						formatterlinks.put(textfield2, f);	
						gridpane2.getChildren().add(textfield2);
					}
					
					textfield.requestFocus();
				}
				TextField finallink = textfield;
				TextField finallink2 = textfield2;
				Button remove = new Button("X");
				remove.setTextFill(Color.BLACK);
				remove.setFont(Font.font(15));
				remove.setOnAction((event2) -> {
					Data.formatters.remove(f);
					
					gridpane2.getChildren().remove(label);
					gridpane2.getChildren().remove(remove);
					
					if(finallink != null){
						formatterlinks.remove(finallink);
						gridpane2.getChildren().remove(finallink);
					}
					
					if(finallink2 != null){
						formatterlinks.remove(finallink2);
						gridpane2.getChildren().remove(finallink2);
					}
					rebuild();
				}); 
				GridPane.setConstraints(remove, pos++, Data.formatters.size());

				gridpane2.getChildren().addAll(label, remove);
				Data.formatters.add(f);
				rebuild();
			}); 
			formatterrow.getChildren().add(but);
		}
		
		Button clearf = new Button("Clear Formatters");
		clearf.setTextFill(Color.RED);
		clearf.setFont(Font.font(12));
		clearf.setPadding(new Insets(5));	
		clearf.setOnAction((event) -> {
			Data.formatters.clear();
			formatterlinks.clear();
			gridpane2.getChildren().clear();
			rebuild();
		}); 
		formatterrow.getChildren().add(clearf);
		
		bottomrows.getChildren().addAll(buttonrow, formatterrow);
		
		pane.setTop(gridpane1);
		pane.setCenter(gridpane2);
		pane.setBottom(bottomrows);
	}
	
	private void setupView(VBox pane){
		maintable = new VBox();
		maintable.setPadding(new Insets(5));
		maintable.setSpacing(4);

		ScrollPane scroll = new ScrollPane();
		scroll.setContent(maintable); 
		scroll.setStyle(".scroll-pane > .viewport {   -fx-background-color: transparent;} .scroll-pane {   -fx-background-color: transparent;}");
		pane.getChildren().add(scroll);
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}